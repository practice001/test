package com.emizr.staticb;

public class StaticBlock {

	{
		System.out.println("StaticBlock.enclosing_method()=====simple block");

	}

	static {
		System.out
				.println("StaticBlock.enclosing_method  =======first static block()");
	}

	public StaticBlock() {

		System.out.println("StaticBlock.StaticBlock()");

	}

	static {

		System.out
				.println("StaticBlock.enclosing_method()================ second block ");

	}

}
