package com.emizr.jdbc;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DbConnect {

	public static void main(String[] args) {

		try {

			// this will create a object of Driver (mysql) class and
			// register that object into DriverManager
			// Class.forName("com.mysql.jdbc.Driver");

			// this will create a object for Driver class
			Driver driver = new com.mysql.jdbc.Driver();

			// this will register in driver manager
			DriverManager.registerDriver(driver);

			// gives you the connection object
			Connection connection = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/demo", "root", "");

			// provide you the platform for execute Query
			Statement statement = connection.createStatement();

			// gives you the date (wrapper)
			String username = "admin";
			ResultSet set = statement
					.executeQuery("select * from tbl_login where username="
							+ username + " and password ='admin'");

			// iterate that data
			while (set.next()) {

				System.out.println("id...." + set.getString("id"));
				System.out.println("name---" + set.getString("username"));
				System.out.println("password===" + set.getString(3));

			}

		} /*
		 * catch (ClassNotFoundException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); }
		 */catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
